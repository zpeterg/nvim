let g:fzf_preview_window = ['right:25%', 'ctrl-_']
"command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, '--hidden', fzf#vim#with_preview(), <bang>0)

function! s:getVisualSelection()
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)

    if len(lines) == 0
        return ""
    endif

    let lines[-1] = lines[-1][:column_end - (&selection == "inclusive" ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]

    return join(lines, "\n")
endfunction

" Search the filename & file, but suppress preview
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, <bang>0)
" Search only the content of the file
command! -bang -nargs=* AgContent call fzf#vim#ag(<q-args>, {'options': '--delimiter : --nth 4.. --prompt "AgContent> "'}, <bang>0)
command! -bang -nargs=? -complete=dir Files call fzf#vim#files(<q-args>, <bang>0)

nnoremap <silent> <leader>s       :Files<CR>
vnoremap <silent> <leader>s       <Esc>:FZF -q <C-R>=<SID>getVisualSelection()<CR><CR>
nnoremap <silent> <leader>g       :AgContent<CR>
nnoremap <silent> <leader>f       :Ag<CR>
nnoremap <silent> <leader>h       :History<CR>
nnoremap <silent> <leader>b       :Buffer<CR>
nnoremap <silent> <leader>l       :BLines<CR>

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <F2> <plug>(fzf-complete-word)
imap <F3> <plug>(fzf-complete-path)
imap <F4> <plug>(fzf-complete-line)

" From https://github.com/jesseleite/dotfiles/blob/c75ae5ebd0589361e1fe84a912f1580a9b1f9a15/vim/plugin-config/fzf.vim#L44-L86
" ------------------------------------------------------------------------------
" # Scoped History Finders
" ------------------------------------------------------------------------------

"command! -bang PHistory call fzf#run(fzf#wrap(s:preview(<bang>0, {
  "\ 'source': s:file_history_from_directory(s:get_git_root()),
  "\ 'options': [
  "\   '--prompt', 'ProjectHistory> ',
  "\   '--multi',
  "\ ]}), <bang>0))

"command! -bang CwdHistory call fzf#run(fzf#wrap(s:preview(<bang>0, {
  "\ 'source': s:file_history_from_directory(getcwd()),
  "\ 'options': [
  "\   '--prompt', 'CwdHistory> ',
  "\   '--multi',
  "\ ]}), <bang>0))

"function! s:file_history_from_directory(directory)
  "return fzf#vim#_uniq(filter(fzf#vim#_recent_files(), "s:file_is_in_directory(fnamemodify(v:val, ':p'), a:directory)"))
"endfunction

"function! s:file_is_in_directory(file, directory)
  "return filereadable(a:file) && match(a:file, a:directory . '/') == 0
"endfunction


" ------------------------------------------------------------------------------
" # Script functions copied from fzf.vim to make above things work
" ------------------------------------------------------------------------------

"function! s:preview(bang, ...)
  "let preview_window = get(g:, 'fzf_preview_window', a:bang && &columns >= 80 || &columns >= 120 ? 'right': '')
  "if len(preview_window)
    "return call('fzf#vim#with_preview', add(copy(a:000), preview_window))
  "endif
  "return {}
"endfunction

"function! s:get_git_root()
  "let root = split(system('git rev-parse --show-toplevel'), '\n')[0]
  "return v:shell_error ? '' : root
"endfunction
