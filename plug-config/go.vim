let g:go_def_mapping_enabled = 0 " leave go-to-definition to coc
let g:go_doc_keywordprg_enabled = 0 " leave doc-lookup to coc
let g:go_fmt_autosave = 0 " leave auto format to coc
let g:go_metalinter_command = "golangci-lint" 
let g:go_metalinter_enabled=[]
let g:go_metalinter_autosave = 1

" Search declarations
nnoremap <silent> <C-p>    :GoDeclsDir<CR>
