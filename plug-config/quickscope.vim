" Trigger a highlight in the appropriate direction when pressing these keys:
"let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

highlight QuickScopePrimary guifg='#00C7DF' ctermfg=155
highlight QuickScopeSecondary guifg='#afff5f' ctermfg=81 

augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' ctermfg=155 
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' ctermfg=81 
augroup END

let g:qs_max_chars=250
