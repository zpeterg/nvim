let g:neoformat_enabled_javascript = ['prettier']
let g:neoformat_enabled_html = ['prettier']
let g:neoformat_enabled_css = ['prettier']
let g:neoformat_enabled_vue = ['prettier']
let g:neoformat_enabled_markdown = ['prettier']
let g:neoformat_try_node_exe = 1
autocmd BufWritePre *.* Neoformat
