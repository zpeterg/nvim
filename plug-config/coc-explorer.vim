let g:coc_explorer_global_presets = {
\   '.vim': {
\     'root-uri': '~/.vim',
\   },
\   'cocConfig': {
\      'root-uri': '~/.config/coc',
\   },
\   'tab': {
\     'position': 'tab',
\     'quit-on-open': v:true,
\   },
\   'tab:$': {
\     'position': 'tab:$',
\     'quit-on-open': v:true,
\   },
\   'floating': {
\     'position': 'floating',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingTop': {
\     'position': 'floating',
\     'floating-position': 'center-top',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingLeftside': {
\     'position': 'floating',
\     'floating-position': 'left-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingRightside': {
\     'position': 'floating',
\     'floating-position': 'right-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'simplify': {
\     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
\   },
\   'buffer': {
\     'sources': [{'name': 'buffer', 'expand': v:true}]
\   },
\ }

:nmap <space>e <Cmd>CocCommand explorer --width 50<CR>
:nmap <space>E <Cmd>CocCommand explorer --no-toggle<CR>
" Use preset argument to open it
nmap <space>d <Cmd>CocCommand explorer --preset .vim<CR>
nmap <space>f <Cmd>CocCommand explorer --preset floating<CR>
nmap <space>c <Cmd>CocCommand explorer --preset cocConfig<CR>
nmap <space>b <Cmd>CocCommand explorer --preset buffer<CR>
nmap <space>B <Cmd>CocCommand explorer --preset buffer --no-toggle<CR>

" List all presets
nmap <space>l <Cmd>CocList explPresets<CR>

