# Reference: https://thevaluable.dev/tmux-config-mouseless/
bind-key h if-shell -F "#{window_zoomed_flag}" 'select-pane -L; resize-pane -Z' 'select-pane -L'
bind-key l if-shell -F "#{window_zoomed_flag}" 'select-pane -R; resize-pane -Z' 'select-pane -R'
bind-key j if-shell -F "#{window_zoomed_flag}" 'select-pane -D; resize-pane -Z' 'select-pane -D'
bind-key k if-shell -F "#{window_zoomed_flag}" 'select-pane -U; resize-pane -Z' 'select-pane -U'

bind-key o split-window -h -c "#{pane_current_path}"
bind-key e split-window -v -c "#{pane_current_path}"

bind-key r source-file ~/.tmux.conf \; display-message "~/.tmux.conf reloaded."

#bind -n C-z resize-pane -Z
bind-key -r C-h resize-pane -L 3
bind-key -r C-l resize-pane -R 3
bind-key -r C-j resize-pane -D 3
bind-key -r C-k resize-pane -U 3

set -sg escape-time 0
set-window-option -g mode-keys vi

# Open add keybind to open output in NVIM
setenv -g PATH "$HOME/bin:$PATH"
set-option -g history-limit 50000

# Vim-edit of history
bind-key v run-shell "~/.config/nvim/vim-edit-tmux-output.sh"

# Clear history
bind-key u clear-history

# Change Select from Space->v
unbind -T copy-mode-vi Space; #Default for begin-selection
bind -T copy-mode-vi v send-keys -X begin-selection
# Change Copy from Enter->y
unbind -T copy-mode-vi Enter; #Default for copy-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "if type \"pbcopy\"; then pbcopy; else xsel --clipboard; fi"

set -g default-terminal "screen-256color"
bind-key C-b last-window

# Find window
#bind -n C-w run-shell -b "tmux list-windows -F \"##I:##W\" | fzf-tmux | cut -d \":\" -f 1 | xargs tmux select-window -t"
bind-key w display-popup -E "tmux list-windows | grep -v \"^$(tmux display-message -p '#S')\$\" | fzf --reverse | cut -d \":\" -f 1 | xargs tmux select-window -t"
# Session switching: https://waylonwalker.com/tmux-fzf-session-jump/
# bind C-w display-popup -E "tmux list-sessions | sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | fzf --reverse | xargs tmux switch-client -t"

source-file "~/.config/nvim/tmux-gruvbox-dark.conf"
