# Setup

## Assumptions

1. That development projects are Nodejs-based, and are located at ~/dev

## Install supporting

### Install Node

#### NVM

1. `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash` (or pipe to `| ash`, etc as needed)
1. `nvm install node` (If you get `nvim command not found`, try `source ~/.bashrc`)
1. `cp ~/.config/nvim/nvm-default-packages $NVM_DIR/default-packages`

### Other

1. `sudo apt install zsh git tmux fzf silversearcher-ag watchman xsel -y`
   Or on Mac: `brew install tmux neovim fzf the_silver_searcher watchman`
1. If tmux <3.3, built from source:
   ```bash
   sudo apt install libevent-dev libncurses-dev autotools-dev automake byacc -y
   git clone https://github.com/tmux/tmux.git
   cd tmux
   sh autogen.sh
   ./configure make && sudo make install
   ```
1. To get latest neovim:
   ```bash
   mkdir ~/Applications
   cd ~/Applications
   curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
   chmod u+x nvim.appimage
   echo 'alias nvim="$HOME/Applications/nvim.appimage"' >> ~/.zshrc
   ```
1. If on bash, change to zsh: 1. chsh -s $(which zsh) 1. Logout and login to use zsh
1. Setup FZF keybindings by pasting this in .zshrc:
   `bash echo "source /usr/share/doc/fzf/examples/key-bindings.zsh" >> ~/.zshrc echo "source /usr/share/doc/fzf/examples/completion.zsh" >> ~/.zshrc `
   Or on Mac: `$(brew --prefix)/opt/fzf/install`
1. `git clone https://gitlab.com/zpeterg/nvim.git ~/.config/nvim`
1. `echo "source-file ~/.config/nvim/.tmux.conf" > ~/.tmux.conf`
1. `echo "source ~/.config/nvim/.bashrc" >> ~/.zshrc`
1. In neovim, run `:PlugInstall` and restart
1. Install npm globals:
   `bash npm install -g prettier npm install -g bash-language-server npm install -g typescript-language-server npm install -g vscode-langservers-extracted `

### Install Hack font

1.  Go here: https://github.com/source-foundry/Hack
1.              ```bash
                cd ~/Downloads
                wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.1/Hack.zip
                unzip Hack.zip -d hack
                cd hack
                mkdir -p ~/.local/share/fonts
                cp *.ttf ~/.local/share/fonts/
                fc-cache -f -v
                fc-list | grep "Hack"

        ```

1.  Change to Hack font in the terminal

## Use

### Vim

`vim` or `nvim`
(To use original vim: `/vim`

### Start tmux project

1. `tmux-start my-project`
1. `tmux attach -t my-project`

## Debugging

- If "tsserver failed after 5 retries" messages: Make sure that current node (via nvm or whatever)
  has typescript installed (`npm install -g typescript`).

# Keyboard shortcuts

## EDIT

|         Action          | shortcut                            |
| :---------------------: | :---------------------------------- |
|  Insert at end of line  | $a                                  |
| Insert at start of line | ^i                                  |
|       Select word       | viw                                 |
|     Select to quote     | vi'                                 |
|    Select inclusive     | va'                                 |
|    Replace to quote     | ci'                                 |
|    Replace inclusive    | ca'                                 |
|       Select line       | V                                   |
|     Duplicate line      | yyp                                 |
|        Cut line         | dd/cc (or dk -upward; dj -downward) |
|       Delete line       | ,dd (or ,dk -upward; ,dj -downward) |
|    Cut line to right    | D/C                                 |
|    Cut line to left     | d^                                  |
|        Cut word         | diw/ciw                             |
| Comment-out line/block: | ,c SPACE                            |
|          Copy           | y                                   |
|          Paste          | p                                   |
|   Paste (insert mode)   | CTRL-o p                            |
|     Paste last item     | "0p                                 |
|    Copy to clipboard    | "+y                                 |
|    Insert line after    | o                                   |
|        Refactor         | \rn                                 |
|    Replace character    | r                                   |
|    Delete character     | x                                   |

## NAVIGATE

|            Action            | shortcut                          |
| :--------------------------: | :-------------------------------- |
|         Half-page up         | CTRL-u                            |
|        Half-page down        | CTRL-d                            |
|    Search case-sensitive     | /\Csearchphrase                   |
|    Clear search highlight    | CTRL-c                            |
|      Search word (down)      | \*                                |
|       Search word (up)       | #                                 |
|        Search/replace        | :s/searchForMe/replaceWithMe      |
|     Search/replace word      | F12                               |
| Search/replace in selection  | :'<,'>s/searchForMe/replaceWithMe |
|         Regex search         | `/\vThis(.*)ThenThis`             |
|      Center screen vert      | zz                                |
|      Center screen horz      | z.                                |
|      Go to top of file       | gg                                |
|      Go to end of file       | G                                 |
|       Copy all of file       | gg999yy                           |
|  Go to back one edit-point   | g;                                |
|    Go forward edit-point     | 3g,                               |
|    Go up/down by # lines     | 10j/10k                           |
|    Go to last/next error     | [g/]g                             |
|       Show error popup       | ,e                                |
|       List all errors        | ,E                                |
|        Fold selection        | zf                                |
|        Unfold/Refold         | zn/zN                             |
|       Delete all folds       | zE                                |
| Go to matching bracket/brace | %                                 |

## INFO/ASSIST

|        Action         | shortcut         |
| :-------------------: | :--------------- |
|    Find definition    | gd               |
|    Go back from gd    | CTRL-o           |
|  Find implementation  | gi               |
|    Get information    | SHIFT-k          |
|      Action menu      | ,ac              |
| Action menu for line  | ,aj              |
|   Quickfix current    | ,qf              |
| Auto-suggest (i mode) | CTRL-space       |
|  Open QuickFix menu   | :copen           |
|  Close QuickFix menu  | :cclose          |
|  Re-apply Vim config  | :source $MYVIMRC |

## FILES & BUFFERS

|              Action              | shortcut |
| :------------------------------: | :------- |
|      Switch to last buffer       | CTRL-^   |
|       Close current buffer       | :bd      |
|            Find file             | ,s       |
|      Find content in files       | ,g       |
|        Find text in files        | ,f       |
|      Find in current buffer      | ,l       |
|       Find in all buffers        | ,b       |
|           Find keymap            | ,Tab     |
| In find view, recall last search | CTRL-p   |
|        Show file history         | ,h       |
|       Close other buffers:       | :BufOnly |
|         Open file list:          | SPACE-e  |
|         Focus file list:         | SPACE-E  |
|        Open buffer list:         | SPACE-b  |
|        Focus buffer list:        | SPACE-B  |

### Explorer

|    Action     | shortcut |
| :-----------: | :------- |
|  - New File   | a        |
| New directory | A        |
| Copy filename | yn       |
|    Delete     | df       |
|   Copy file   | yy       |
|   Cut file    | dd       |
|  Paste file   | p        |
|    Rename     | r        |
|  Show hidden  | zh       |
|    Refresh    | R        |

## TMUX

|         Action          | shortcut              |
| :---------------------: | :-------------------- |
|     Split vertical      | CTRL-b e              |
|    Split horizontal     | CTRL-b o              |
|  Switch panes up/down   | CTRL-b k/j            |
| Switch panes left/right | CTRL-b h/l            |
|   Resize pane up/down   | CTRL-b CTRL-k/j       |
| Resize pane left/right  | CTRL-b CTRL-h/l       |
|       Close pane        | CTRL-b x              |
|      Edit in NVIM       | CTRL-b v              |
|   Switch to edit mode   | CTRL-b [              |
|      Clear history      | CTRL-b u              |
|      Rename window      | CTRL-b ,              |
|   Rearrange workspace   | CTRL-b space          |
| Rotate panes clockwise  | CTRL-b CTRL-o         |
| Move pane clock/counter | CTRL-b {/}            |
|      List commands      | CTRL-b :list-commands |
|       Last window       | CTRL-b CTRL-b         |
|      Search window      | CTRL-b w              |
|        Last pane        | CTRL-b ;              |

# Tips

## Debugging

https://pragmaticpineapple.com/7-ways-to-debug-jest-tests-in-terminal/
For Create React App: https://pragmaticpineapple.com/7-ways-to-debug-jest-tests-in-terminal/#5-what-about-cra

1. Open chrome://inspect
1. Click "Open dedicated DevTools for Node"
1. In left panel, click Filesystem and open the project root
1. Navigate into file to set breakpoint
1. In Terminal, run `react-scripts --inspect test --runInBand --no-cache`
