alias vim='nvim'
export EDITOR="$HOME/Applications/nvim.appimage"
alias v='nvim `fzf`'
alias vs='nvim `ag . --hidden --ignore .git | fzf | sed "s/:.*//"`'
alias gc='git checkout $(git branch --sort=-committerdate | fzf)'
alias tmux-start='~/.config/nvim/tmux-start.sh'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='ag . -l --hidden --ignore .git'
export FZF_DEFAULT_OPTS="--history=$HOME/.fzf_history"

if [[ $TMUX_PANE ]]; then
  HISTFILE=$HOME/.bash_history_tmux_${TMUX_PANE:1}
fi

bindkey -M viins '^?' backward-delete-char
bindkey -M viins '^H' backward-delete-char

export PROMPT='%B%F{240}%3~%f%b %# '
bindkey -v
if [[ $TERM == xterm ]]; then TERM=xterm-256color; fi

# To present NVIM TS LSP "javascript heap out of memory" error
export NODE_OPTIONS=--max-old-space-size=4096
