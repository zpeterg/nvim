#!/bin/bash

project_name=$1

if [ ! "$project_name" ]; then
        echo "Please provide a project name"
        exit 1
fi

tmux new-session -d -s $project_name

window=0

tmux split-pane -v -t $project_name:$window
tmux split-pane -h -t $project_name:$window.bottom
tmux resize-pane -t $project_name:$window.top -y 70%
tmux resize-pane -t $project_name:$window.bottom-right -x 70%

tmux send-keys -t $project_name:$window.top "cd ~/dev/$project_name" Enter 'vim' Enter

tmux send-keys -t $project_name:$window.bottom-left "cd ~/dev/$project_name" Enter 'npm run start' Enter

tmux send-keys -t $project_name:$window.bottom-right "cd ~/dev/$project_name" Enter 'clear' Enter

