" curl -o fLO $HOME/.config/nvim/autoload/plug.vim \
" 	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
let mapleader = ","
filetype plugin indent on
syntax on

call plug#begin('~/.config/nvim/plugged')

"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox', {'commit': 'bf2885a95efdad7bd5e4794dd0213917770d79b7'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdcommenter', {'commit': 'f8671f783baeb0739f556d9b6c440ae1767340d6'}
Plug 'unblevable/quick-scope', {'commit': '5e2373e36d774e1cebd58b318346db32c52db21a'}
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'puremourning/vimspector'
Plug 'ggandor/leap.nvim', {'commit': 'a9949044bc59b0ae026c2e8394da826069049211'}
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip', {'commit': '0abfa1860f5e095a07c477da940cfcb0d273b700'}
Plug 'hrsh7th/vim-vsnip', {'commit': '8f199ef690ed26dcbb8973d9a6760d1332449ac9'}
Plug 'sbdchd/neoformat'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'kyazdani42/nvim-web-devicons', {'commit': '2d02a56189e2bde11edd4712fea16f08a6656944'}
Plug 'kyazdani42/nvim-tree.lua', {'commit': '011a7816b8ea1b3697687a26804535f24ece70ec'}
Plug 'xiyaowong/nvim-cursorword', {'commit': 'c0f2958ec729b47be2dba0d79ef43d005dac9f4e'}

call plug#end()


"let g:coc_global_extensions = ['coc-json@1.4.1', 'coc-prettier@9.3.1', 'coc-tsserver@1.10.5', 'coc-eslint@1.5.8', 'coc-explorer@0.22.9', 'coc-html@1.6.1', 'coc-go@1.3.0', '@yaegassy/coc-volar@0.24.0']
"let g:coc_global_extensions = ['coc-explorer@0.22.9', '@yaegassy/coc-volar@0.24.0']


let g:ackprg = 'ag --nogroup --nocolor --column'

autocmd vimenter * ++nested colorscheme gruvbox

" Keep cursor in vertical center
" set so=999
augroup KeepCentered
  autocmd!
  autocmd CursorMoved * normal! zz
augroup END
inoremap <CR> <C-\><C-O><C-E><CR>
inoremap <BS> <BS><C-O>zz
nnoremap o <C-E>o

"command! -nargs=0 Prettier :CocCommand prettier.formatFile

" Undo/redo
nnoremap <C-Z> u
nnoremap <C-Y> <C-R>
inoremap <C-Z> <C-O>u
inoremap <C-Y> <C-O><C-R>

" Remap HJKL to navigate in insert/command
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>
nnoremap <C-h> <Left>
nnoremap <C-j> <Down>
nnoremap <C-k> <Up>
nnoremap <C-l> <Right>
" Search/replace word under cursor
nnoremap <f12> :%s/<c-r><c-w>/<c-r><c-w>/gc<c-f>$F/i
" Clear highlights on CTRL-c
nnoremap <nowait><silent> <C-C> :noh<CR>
command! BufOnly execute '%bdelete|edit #|normal `"'
" Scroll horizontally
nnoremap <silent> z. :<C-u>normal! zszH<CR>
" Delete without copy
nnoremap <leader>d "_d
xnoremap <leader>d "_d
" Paste without copy
nnoremap <leader>p "0p
xnoremap <leader>p "0p

"source $HOME/.config/nvim/plug-config/coc.vim
"source $HOME/.config/nvim/plug-config/coc-explorer.vim
source $HOME/.config/nvim/plug-config/fzf.vim
source $HOME/.config/nvim/plug-config/quickscope.vim
source $HOME/.config/nvim/plug-config/go.vim
source $HOME/.config/nvim/plug-config/vimspector.vim
source $HOME/.config/nvim/plug-config/leap.vim
source $HOME/.config/nvim/plug-config/nvm-lspconfig.vim
source $HOME/.config/nvim/plug-config/nvm-cmp.vim
source $HOME/.config/nvim/plug-config/neoformat.vim
source $HOME/.config/nvim/plug-config/treesitter.vim
source $HOME/.config/nvim/plug-config/nvim-tree.vim
source $HOME/.config/nvim/plug-config/nvim-cursorword.vim


set colorcolumn=100
set background=dark
set nowrap
set smartcase
set hlsearch
set noerrorbells
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2
autocmd FileType typescriptreact setlocal shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType javascriptreact setlocal shiftwidth=2 tabstop=2
set expandtab
set autoindent
set smartindent
set statusline="%f%m%r%h%w [%Y] [0x%02.2B]%< %F%=%4v,%4l %3p%% of %L"
set number relativenumber
set signcolumn=no
" set / search to be case-insensitive, unless capitals are used
set ignorecase
set smartcase
set cursorline

